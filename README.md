# Assignment for Full Stack Candidates

## Your Task
Create a prototype of ordering system for MOX modules.

## What is MOX?
MOX is a world-first modular router made by the Turris team. To get more info about MOX please see the
[MOX page](https://www.turris.cz/en/mox/overview/). You can also play with 
[MOX configurator](https://mox-configurator.turris.cz/) but knowledge about modules compatibility is **not** required to
complete this task.

MOX can be built from a bunch of modules. Some of these modules can be extended with submodules. Your task is to create a prototype of the ordering system of MOX modules.

## Requirements
### Functional Requirements
 * Create "ordering" page that lets user add and remove modules to/from an order.
 * If module can be extended with submodules then the user should be able to select submodules as well.
 * User should be able to select more modules per config.
 * User should not be able to select more submodules per each module.
 * User can clear the order using "reset" button on "ordering" page.
 * After submitting the form user will see "confirmation" page with "confirm" and "go back" (to "ordering" page) functions.
 * After confirming the order user will see confirmation message saying that the order will be processed shortly.

### Non Functional Requirements
 * You can use any of the JS frameworks or write it in vanilla (pure) JS.
 * The solution must be a single page application.
 * Your solution should be extensively covered by tests. Any testing framework is welcome.
 * Provide instructions on how to run the solution.

## Backend
Backend is running on https://mox-ordering.test.turris.cz/.

### API Endpoints
There are two endpoints:
* `modules` - handles list of modules and submodules - accepts GET method only,
* `orders` - handles list of orders - accepts POST methods only.

`orders` endpoint accepts data in JSON format, e.g.:
```json
{
    "modules": [1, 2, 3],
    "submodules": [1, 2, 3]
}
```
`modules` and `submodules` lists contain IDs of ordered products.

Please check API docs by link https://mox-ordering.test.turris.cz/ for more info.
